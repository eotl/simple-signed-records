#! /bin/bash
#
# Requires jq and jo packages

URL="http://127.0.0.1:9999"

function testTesterson() {
    if [[ $1 == $2 ]]; then
        RES="passes"
    else
        RES="fails"
    fi
    echo $RES
}

function signRequest() {
    PAYLOAD=$(jo Nonce="" Path="" Challenge="${1}")
    local SSR_CERT=$(echo $PAYLOAD | ../cmd/ssr/ssr sign -p testkey.ed25519.key | base64 -w 0)
    echo $SSR_CERT
}

# Test 1 - unsigned unprotected request
PATH_1="data.json"
echo -e "Fetch: ${URL}/${PATH_1}"
TEST_1=$(curl -s -I $URL/$PATH_1 | jq '.msg')
RESULT_1=$(testTesterson "This request should succeed" $TEST_1)
echo -e "Test 1: $RESULT_1\n"

# Test 2 - unsigned protected path
PATH_2="private.json"
echo -e "Fetch: ${URL}/${PATH_2}"
TEST_2=$(curl -s -I $URL/$PATH_2 | grep 'HTTP')
#echo -e $TEST_2
RESULT_2=$(testTesterson "HTTP/1.1 401 Unauthorized" $TEST_2)
echo -e "Test 2: $RESULT_2\n"


# Test 3 - unsuccessfully signed protected path
PATH_3="successful.txt"
echo -e "Fetch: ${URL}/${PATH_3}"
CHALLENGE=$(curl -S -s -i -k -q $URL/$PATH_3 -H "Www-authenticate" | grep 'Www-Authenticate' | awk {'print $2'} | jq '.Challenge')
SSR_CERT=$(signRequest $CHALLENGE)
echo "SSR_CERT: ${SSR_CERT}"

TEST_3=$(curl -s -H "Ssr-Authenticate: ${SSR_CERT}" $URL/$PATH_3)
RESULT_3=$(testTesterson "Hooray for a successful SSR" $TEST_3)
echo -e "Test 3: $RESULT_3"

# Test 4 - successfully signed protected path
PATH_FILE1="files/message-1.txt"
echo -e "Fetch: ${URL}/${PATH_FILE1}"
curl -s -I $URL/$PATH_FILE1 | grep 'HTTP/1.1'
