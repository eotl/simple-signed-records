import nacl from 'tweetnacl';
import naclUtil from 'tweetnacl-util';
import { ssr, ssrIdentity, ssrFetcher } from './index.js';

// Data
const keypair = nacl.sign.keyPair()
const keypair2 = nacl.sign.keyPair()
const identityKey = ssr.encodeIdentityKey(keypair) 
const record = { message: "hackers of the world", action: "unite", status: "booting" }

let signedRecord = null

test("Sign an example record", () => {
    signedRecord = ssr.sign({ record, keypair });
    expect(signedRecord).toBeTruthy();
})

test("Verify SSR of example record", () => {
    expect(ssr.verify(signedRecord)).toBeTruthy();
})

test("Extract data from an SSR", () => {
    expect(ssr.data(signedRecord)).toEqual({
        data: record,
        identities: [ identityKey ]
    });
})

test("Add signature to an existing SSR", () => {
    signedRecord = ssr.addSignature(signedRecord, keypair2);
    expect(signedRecord).toBeTruthy();
})

/*
test("Verify an SSR with additional signature", () => {
    expect(ssr.verify(signedRecord)).toBeTruthy();
})

test("Extract data from an SSR with additional signature", () => {
    expect(ssr.data(signedRecord)).toEqual({
        data: record,
        identities: [
            naclUtil.encodeBase64(keypair.publicKey),
            naclUtil.encodeBase64(keypair2.publicKey)
        ]
    });
})
*/
