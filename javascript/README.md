Simple Signed Records JS
========================

## Install

```
$ yarn add @eotl/simple-signed-records
```

For basic functionality, include the `SSR` modules in your code:

```
import { ssr } from 'simple-signed-records'

ssr.sign({ record, keypair });
```

## Identity

To offer user-friendly identity functionality (via a "seed phrase"), you should also expose
the `ssrIdentity` method.

```
import { ssr, ssrIdentity } from 'simple-signed-records'
```

To create new account keys, do the following

```
const identity = ssrIdentity.createIdentity()
```

To restore an account from 16 word seed, do the following

```
const identity = ssrIdentity.restoreIdentity(words)
```


## Signing Records

To create a SSR use the following code

```
const record = { ... }
const validSeconds = 15778800
let secretKey = decodeBase64(secretKey)
let publicKey = decodeBase64(publicKey)

let signedData = ssr.sign({
    record,
    keypair: { secretKey, publicKey },
    validSeconds,
});
```


## Verifying Records

Tbw...


## Identity Certificate

To generate a certificate for GET requests, use the following

```
const certificate = ssrIdentity.generateCert(identity);
```
