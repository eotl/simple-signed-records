SSR Command
===========

CLI is used to sign, verify, and make Simple Signed Requests.

```
$ ssr -help

Usage of ssr:
ssr <command> [arguments]

The commands are:

fetch [URL]
  -k string
        Private key file, must be ed25119 
  -o string
        Path to write response to

verify
  -p string
    Public key file, must be ed25519

sign
  -p string
    Private key file, must be ed25519 (default "privkey")

keys
  file path to ssh public key file with ed25519 key
```

### Fetch

To make a HTTP request which requires a valid SSR use the `fetch` sub-command. This 

```
$ ssr fetch http://localhost:9999/images/collaboration.jpg
```

### Sign

```
$ echo "Hooray for crypto, as in cryptography" | ssr sign -p ~/.ssh/id_ed25519

{"Version":0,"Expiration":1659751115,"KeyType":"ed25519","Certified":"SG9vcmF5IGZvciBjcnlwdG8sIGFzIGluIGNyeXB0b2dyYXBoeQo=","Signatures":[{"Identity":"ssi1hapvd40ly38fkcazny5czsp7qkay334wvq5udzx0zvyk78v5trmqj2gjj2","Payload":"NYY61aTkAVo2mgYyzCL/tb0PWxGfckEiPOtO8jWW+VhTanSrLZP6+hr462tarzsLygaw8UjgSsuGRqqtaMsPAg=="}]}
```

### Verify

Success:

```
$ echo "Hooray for crypto, as in cryptography" | ssr sign -p private.key | ssr verify -p public.key
Hooray for crypto, as in cryptography
```

Failure:

```
$ echo "Hooray for crypto, as in cryptography" | ssr sign -p private.key | ssr verify -p wrongkey.pub
2022/02/04 08:59:50 Request could not be verified: failure to find signature associated with the given identity
```

### Invite

The `invite` subcommand is used to generate invite tokens.

```
$ ssr invite 
Each word represents 11.000000 bits of entropy
orbit-goat-airport-session
```

This produces a simple text file which is a sha256 hash of the token containing
`/` character.

```
$ ls
f91a4ec328f185693bc38053b4da30985e91e85bccbb7d10769aac49a49d87e3
```

Putting this file in an `invites/` directory that `ssrd` is configured to point
to would make the entire site only grant access to requests signed a key that
successfully redeems that token.

```
$ ssr invite -a /files/secret-mixtape.mp3 -o /path/to/site/.ssr/invites/
```


### Redeem

The `redeem` subcommand is used to redeeming invite tokens.

```
$ ssr redeem https://your-site.com magical-invite-tokens-rule
```

