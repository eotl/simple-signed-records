SSRD Server
===========

To run an HTTP server that uses SSRs to authenticate requests, use the `ssrd`
command in this package. It can serve static files or run as a proxy.

```
$ ssrd
Usage of ssrd:
  -config string
    	Permission configuration file
  -d string
    	URL of upstream host to proxy requests for, e.g. http://127.0.0.1
  -dirlist
    	Enable directory listing mode
  -l int
    	Listening port
  -p int
    	Proxy listening port, disabled by default
  -path string
    	Siteroot to serve from, current directory by default (default ".")
```

### Serving Files

The command `ssrd` launches a webserver and uses SSRs to authenticate requests

```
$ ssrd -path /path/to/your/html/ -l 9999
```

### Configuration

The default configuration file is located at `.ssr/config` and contains contents like:

```
cors_origins = ["*"]
cors_methods = ["GET", "HEAD", "POST"]
http_responses = "json"
dirlist = true
http_port = 9999
invite_dir = "invites"
#proxy_to = "http://127.0.0.1:8080"
#proxy_port = 8000
autocert_domains = []
```

To run `ssrd` as a proxy sever, uncomment the following lines.

```
proxy_to = "http://127.0.0.1:8080"
proxy_port = 8000
```

To auomatically generate LetsEncrypt certificates, specify the domain names:

```
autocert_domains = ["foo.com", "example.com"]
```

### Access Control List

And the default access control file is located at `.ssr/auth` and contains entries such as:

```
ssi1vghn3z9rwupvg202qeu35ukzrrtr59gnxq9cvwp6mlev637vngsqtw86ha /sign.go r
ssi1vghn3z9rwupvg202qeu35ukzrrtr59gnxq9cvwp6mlev637vngsqtw86ha /foo/* r
...
```

The format of the access control configuration file is:

```
<IdentityKey> <Path> <Mode>
```

- `IdentityKey` is a [bech32](https://en.bitcoin.it/wiki/Bech32) encoded `ed25519` public key which is *always* prefixed with `ssi...`
- `Path` is the URLPath of the resource under the siteroot, and supports wildcard and recursive wildcard pattern
- `Mode` is the read `r` or write `w` flag. Presently only `r` mode is supported, due to security concerns.


### Custom Locations

The location of the configuration file may alternately be specified by the `-config` flag.

```
ssrd listen -path ./testing/website/ -config /some/other/path
```

If the -dirlist flag is specified, ssr will allow browsing of the resources
under siteroot. Restricted paths will follow the rules as given, but note that
the directory listing does *NOT* hide the existence of protected paths.

If an `index.html,htm` exists in the directory, that will be preferentially served.

