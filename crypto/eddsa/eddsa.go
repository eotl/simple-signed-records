// eddsa.go - EdDSA wrappers.
// Copyright (C) 2017  Yawning Angel.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package eddsa provides EdDSA (Ed25519) wrappers.
package eddsa

import (
	"crypto/subtle"
	"errors"
	"fmt"
	"io"

	"codeberg.org/eotl/simple-signed-records/utils/encoding/bech32"
	"golang.org/x/crypto/ed25519"
)

const (
	// PublicKeySize is the size of a serialized PublicKey in bytes (32 bytes).
	PublicKeySize = ed25519.PublicKeySize

	// PrivateKeySize is the size of a serialized PrivateKey in bytes (64 bytes).
	PrivateKeySize = ed25519.PrivateKeySize

	// SignatureSize is the size of a serialized Signature in bytes (64 bytes).
	SignatureSize = ed25519.SignatureSize

	keyType = "ed25519"
)

var errInvalidKey = errors.New("eddsa: invalid key")

// PublicKey is a EdDSA public key.
type PublicKey struct {
	pubKey       ed25519.PublicKey
	bech32String string
}

// InternalPtr returns a pointer to the internal (`golang.org/x/crypto/ed25519`)
// data structure.  Most people should not use this.
func (k *PublicKey) InternalPtr() *ed25519.PublicKey {
	return &k.pubKey
}

// Bytes returns the raw public key.
func (k *PublicKey) Bytes() []byte {
	return k.pubKey
}

// Identity returns the key's identity, in this case it's our
// public key in bytes.
func (k *PublicKey) Identity() string {
	encoded, _ := bech32.ConvertBytesToBech32(k.Bytes())
	return encoded
}

// ByteArray returns the raw public key as an array suitable for use as a map
// key.
func (k *PublicKey) ByteArray() [PublicKeySize]byte {
	var pk [PublicKeySize]byte
	copy(pk[:], k.pubKey[:])
	return pk
}

// FromBytes deserializes the byte slice b into the PublicKey.
func (k *PublicKey) FromBytes(b []byte) error {
	if len(b) != PublicKeySize {
		return errInvalidKey
	}

	k.pubKey = make([]byte, PublicKeySize)
	copy(k.pubKey, b)
	k.rebuildBech32String()
	return nil
}

// MarshalBinary implements the BinaryMarshaler interface
// defined in https://golang.org/pkg/encoding/
func (k *PublicKey) MarshalBinary() ([]byte, error) {
	return k.Bytes(), nil
}

// UnmarshalBinary implements the BinaryUnmarshaler interface
// defined in https://golang.org/pkg/encoding/
func (k *PublicKey) UnmarshalBinary(data []byte) error {
	return k.FromBytes(data)
}

// MarshalText implements the TextMarshaler interface
// defined in https://golang.org/pkg/encoding/
func (k *PublicKey) MarshalText() ([]byte, error) {
	//return []byte(base64.StdEncoding.EncodeToString(k.Bytes())), nil
	encoded, err := bech32.ConvertBytesToBech32(k.Bytes())
	return []byte(encoded), err
}

// UnmarshalText implements the TextUnmarshaler interface
// defined in https://golang.org/pkg/encoding/
func (k *PublicKey) UnmarshalText(data []byte) error {
	return k.FromString(string(data))
}

// FromString deserializes the string s into the PublicKey.
func (k *PublicKey) FromString(s string) error {
	raw, err := bech32.DecodeBech32ToBytes(s)
	if err == nil {
		return k.FromBytes(raw)
	}
	return fmt.Errorf("eddsa: key is not bech32")
}

// Verify returns true iff the signature sig is valid for the message msg.
func (k *PublicKey) Verify(sig, msg []byte) bool {
	return ed25519.Verify(k.pubKey, msg, sig)
}

// String returns the public key as a bech32 encoded string.
func (k *PublicKey) String() string {
	return k.bech32String
}

func (k *PublicKey) rebuildBech32String() {
	encoded, _ := bech32.ConvertBytesToBech32(k.Bytes())
	k.bech32String = encoded
}

// Equal returns true iff the public key is byte for byte identical.
func (k *PublicKey) Equal(cmp *PublicKey) bool {
	return subtle.ConstantTimeCompare(k.pubKey[:], cmp.pubKey[:]) == 1
}

// PrivateKey is a EdDSA private key.
type PrivateKey struct {
	pubKey  PublicKey
	privKey ed25519.PrivateKey
}

// InternalPtr returns a pointer to the internal (`golang.org/x/crypto/ed25519`)
// data structure.  Most people should not use this.
func (k *PrivateKey) InternalPtr() *ed25519.PrivateKey {
	return &k.privKey
}

// FromBytes deserializes the byte slice b into the PrivateKey.
func (k *PrivateKey) FromBytes(b []byte) error {
	if len(b) != PrivateKeySize {
		return errInvalidKey
	}

	k.privKey = make([]byte, PrivateKeySize)
	copy(k.privKey, b)
	k.pubKey.pubKey = k.privKey.Public().(ed25519.PublicKey)
	k.pubKey.rebuildBech32String()
	return nil
}

// Bytes returns the raw private key.
func (k *PrivateKey) Bytes() []byte {
	return k.privKey
}

// MarshalBinary implements the BinaryMarshaler interface
// defined in https://golang.org/pkg/encoding/
func (k *PrivateKey) MarshalBinary() ([]byte, error) {
	return k.Bytes(), nil
}

// UnmarshalBinary implements the BinaryUnmarshaler interface
// defined in https://golang.org/pkg/encoding/
func (k *PrivateKey) UnmarshalBinary(data []byte) error {
	return k.FromBytes(data)
}

// Identity returns the key's identity, in this case it's our
// public key in bytes.
func (k *PrivateKey) Identity() string {
	return k.PublicKey().Identity()
}

// KeyType returns the key type string,
// in this case the constant variable
// whose value is "ed25519".
func (k *PrivateKey) KeyType() string {
	return keyType
}

// PublicKey returns the PublicKey corresponding to the PrivateKey.
func (k *PrivateKey) PublicKey() *PublicKey {
	return &k.pubKey
}

// Sign signs the message msg with the PrivateKey and returns the signature.
func (k *PrivateKey) Sign(msg []byte) []byte {
	return ed25519.Sign(k.privKey, msg)
}

// NewKeypair generates a new PrivateKey sampled from the provided entropy
// source.
func NewKeypair(r io.Reader) (*PrivateKey, error) {
	pubKey, privKey, err := ed25519.GenerateKey(r)
	if err != nil {
		return nil, err
	}

	k := new(PrivateKey)
	k.privKey = privKey
	k.pubKey.pubKey = pubKey
	k.pubKey.rebuildBech32String()
	return k, nil
}
