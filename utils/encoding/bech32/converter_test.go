package bech32

import (
	"encoding/base64"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestConvertBytesToBech32(t *testing.T) {
	assert := assert.New(t)
	data := []byte("This is a test")
	result, err := ConvertBytesToBech32(data)
	require.NoError(t, err, "ConvertBytesToBech32()")
	expected := "ssi1235xjueqd9ejqcfqw3jhxaqvk0vzg"
	assert.Equal(expected, result, "ConvertBytesToBech32()")
}

func TestConvertBase64ToBech32(t *testing.T) {
	assert := assert.New(t)
	data := []byte("This is a test")
	b64string := base64.StdEncoding.EncodeToString(data)
	result, err := ConvertBase64ToBech32(b64string)
	require.NoError(t, err, "ConvertBase64ToBech32()")
	expected := "ssi1235xjueqd9ejqcfqw3jhxaqvk0vzg"
	assert.Equal(expected, result, "ConvertBase64ToBech32()")
}

func TestDecodeBech32ToBytes(t *testing.T) {
	assert := assert.New(t)
	data := "ssr1235xjueqd9ejqcfqw3jhxaqayyyyk"
	expected := []byte("This is a test")
	result, err := DecodeBech32ToBytes(data)
	require.NoError(t, err, "DecodeBech32ToBytes()")
	assert.Equal(expected, result, "DecodeBech32ToBytes()")
}
