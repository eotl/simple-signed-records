Simple Signed Records
=====================

SSRs are a new approach to authorization and verification of data over HTTP based on ed25519 cryptographic keys. This responsitory contains code to generate two binary files:

### `ssr`

- A CLI tool to cryptographically sign and verify arbitrary data as an SSR
- Manage invite tokens to SSR enabled websites
- Fetching SSR authenticated data

### `ssrd`

- An HTTP server that verifies SSRs with cryptographic keys on a per file or directory level for access control
- Multi domain support, whereby content is the same across domains
- TLS cert management


## Installing

```
$ git clone https://codeberg.org/eotl/simple-signed-records
$ cd simple-signed-records/cmd/ssr/
$ go build && cd ../ssrd/ && go build && cd ../
$ cp ssr/ssr ssrd/ssrd ~/.local/bin/
```

Create a service unit for `ssrd` at `/etc/systemd/system/ssrd.service`
with the following

```
[Unit]
Description=SSR Daemon

[Service]
ExecStart=/home/user/.local/bin/ssrd -path /to/your/website
Restart=always

[Install]
WantedBy=multi-user.target
```

## Developing

```
$ git clone https://codeberg.org/eotl/simple-signed-records
$ cd simple-signed-records/
$ make first-run 
```

**Golang**

To develop the Golang code, work within the codebase and run tests when complete.

```
$ make dev-go
```

**JavaScript**

To work on the `ssr.js` module you can run in a terminal

```
$ make serve-js
```

Access the `vite` dev server at: [http://localhost:8090](http://localhost:8090)


**Testing HTTP Server**

To develop either the Golang or JavaScript you will ideally want to verify it in
a browser application as you go and to test the `ssr.js` implementation against
the `ssrd` HTTP server, which should have been built on the `first-run` stage.

To run `ssrd` in one terminal you can run:

```
$ make dev-go
```

To view and test the `ssr.js` JavaScript library against the Golang `ssrd` HTTP server and various access
view the tests in a browser at:

- [http://localhost:8089](http://localhost:8089)

To do ongoing JS dev in another terminal and build on changes, run this:

```
$ make dev-js
```


## Testing

To run tests on the Golang code run:

```
$ make test-go
```

To run tests on the JavaScript code run:

```
$ make test-js
```
