package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"path"
)

var (
	verifyCommand = flag.NewFlagSet("verify", flag.ExitOnError)
	pubKeyPath    = verifyCommand.String("p", "", "Public key file, must be ed25519")
	signCommand   = flag.NewFlagSet("sign", flag.ExitOnError)
	privKeyPath   = signCommand.String("p", defaultKeyPath(), "Private key file, must be ed25519")
	fetchCommand  = flag.NewFlagSet("fetch", flag.ExitOnError)
	fetchKeyPath  = fetchCommand.String("k", defaultKeyPath(), "Private key file, must be ed25519")
	fetchToFile   = fetchCommand.String("o", "", "Path to write response to")
	inviteCommand = flag.NewFlagSet("invite", flag.ExitOnError)
	inviteLang    = inviteCommand.String("l", "en", "Wordlist Language")
	inviteWords   = inviteCommand.Int("w", 4, "Number of words in an invite")
	inviteNum     = inviteCommand.Int("n", 1, "Number of invites to generate")
	inviteAuth    = inviteCommand.String("a", "/", "Auth rule to grant the invited")
	inviteOutput  = inviteCommand.String("o", "", "Path to write the invites to")

	redeemCommand = flag.NewFlagSet("redeem", flag.ExitOnError)
	redeemKeyPath = redeemCommand.String("k", defaultKeyPath(), "Private key file, must be ed25519")
	redeemApiEp   = redeemCommand.String("e", "redeem_token", "Invite redemption API endpoint")
)

func defaultKeyPath() string {
	cfgDir, _ := os.UserConfigDir()
	return path.Join(cfgDir, "ssr/default.ed25519.key")
}

func main() {

	if len(os.Args) < 2 {
		usage()
		os.Exit(-1)
	}
	switch os.Args[1] {
	case "fetch":
		fetchCommand.Parse(os.Args[2:])
		// the only non-flag arg is URL
		nonFlagArgs := fetchCommand.Args()
		if len(nonFlagArgs) != 1 {
			usage()
			return
		}

		fetch(*fetchKeyPath, nonFlagArgs[0], *fetchToFile)
	case "verify":
		verifyCommand.Parse(os.Args[2:])
		verify(*pubKeyPath)
	case "sign":
		signCommand.Parse(os.Args[2:])
		sign(*privKeyPath)
	case "keys":
		keys()
	case "invite":
		inviteCommand.Parse(os.Args[2:])
		if wl, ok := l[*inviteLang]; !ok {
			usage()
		} else {
			writeinvites(makeinvites(wl, *inviteWords, *inviteNum), *inviteAuth, *inviteOutput)
		}
	case "redeem":
		redeemCommand.Parse(os.Args[2:])
		nonFlagArgs := redeemCommand.Args()
		if len(nonFlagArgs) != 2 {
			usage()
			return
		}
		u, err := url.Parse(nonFlagArgs[0]) // url
		if err != nil {
			fmt.Println("Failed to redeem: ", err)
			return
		}
		u.Path = *redeemApiEp
		t := nonFlagArgs[1] // token
		response, err := redeeminvite(*redeemKeyPath, u.String(), t)
		if err != nil {
			fmt.Println("Failed to redeem: ", err)
			return
		}
		fmt.Println(response)
	default:
		usage()
	}
}

func usage() {
	fmt.Println("Usage of ssr:")
	fmt.Println("\tssr <command> [arguments]")
	fmt.Println("\nThe commands are:")
	fmt.Println("\nfetch [URL]")
	fetchCommand.PrintDefaults()
	fmt.Println("\nverify")
	verifyCommand.PrintDefaults()
	fmt.Println("\nsign")
	signCommand.PrintDefaults()
	fmt.Println("\nkeys")
	fmt.Printf("  file\t%s\n", "path to ssh public key file with ed25519 key")
	fmt.Println("\ninvite")
	inviteCommand.PrintDefaults()
	fmt.Println("\nredeem")
	fmt.Println("  URL TOKEN")
}

func stdin() string {
	fi, err := os.Stdin.Stat()
	if err != nil {
		return ""
	}
	fm := fi.Mode()
	switch fm & (os.ModeCharDevice | os.ModeDevice | os.ModeNamedPipe) {
	case os.ModeCharDevice | os.ModeDevice:
		// keyboard
	case os.ModeNamedPipe:
		// pipe
	default:
		return ""
	}

	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		return ""
	}
	return string(bytes)
}
