package main

import (
	"codeberg.org/eotl/simple-signed-records/crypto/eddsa"
	"codeberg.org/eotl/simple-signed-records/server"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
)

func fetch(privKeyPath string, url string, outfile string) {
	var w io.WriteCloser
	var err error
	privKey := new(eddsa.PrivateKey)
	b := *parseSSHPrivate(privKeyPath)
	if b == nil || len(b) == 0 {
		panic("wtf")
	}

	privKey.FromBytes(b)

	ssrClient := server.NewClient(privKey)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	re, err := ssrClient.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}

	// always use outfile if specified, even if it already exists
	if outfile != "" {
		w, err = os.Create(outfile)
		defer w.Close()
		if err != nil {
			fmt.Println(err)
			return
		}
	} else {
		// otherwise see if there is a suggested filename
		fn := re.Header.Get("Content-Disposition")
		if fn != "" {
			// reject anything unclean or with path elements
			if path.Clean(fn) == fn && path.Base(fn) == fn && fn != "." && fn != "/" {
				fmt.Printf("Saving %s to %s\n", url, fn)
				// ONLY create the file if it does not already exist.
				// See: https://stackoverflow.com/questions/23842247/reading-default-filemode-when-using-os-o-create
				// with regard to behavior with umask.
				w, err = os.OpenFile(fn, os.O_CREATE|os.O_EXCL|os.O_RDWR, 0660)
				defer w.Close()
				if err != nil {
					fmt.Printf("Rejecting suggested path/filename: %s\n", err)
					return
				}
			} else {
				fmt.Printf("Rejecting suggested path/filename: %s\n", fn)
				return
			}
		} else {
			// default to stdout
			w = os.Stdout

			/*
				// XXX: if the filetype is binary, writing to stdout is messy
				// if the URL last path element (Base) is a valid filename
				content := re.Header.Get("Content-Type")
				// enumerate types that we don't want to automatically try to save to disk
				if content != "text/plain" && content != "application/json" {
					fn := path.Base(url)
					if fn != "." && fn != "/" && fn != "" {
						w, err = os.Create(fn)
						if err != nil {
							fmt.Println(err)
							return
						}
					}
				}
			*/
		}
	}
	_, err = io.Copy(w, re.Body)
	if err != nil {
		fmt.Println(err)
	}
}
