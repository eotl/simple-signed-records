package main

import (
	"crypto/ed25519"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"codeberg.org/eotl/simple-signed-records/crypto/cert"
	"codeberg.org/eotl/simple-signed-records/crypto/eddsa"
	"golang.org/x/crypto/ssh"
)

func sign(privKeyPath string) {
	var privKey eddsa.PrivateKey
	privKey.FromBytes(*parseSSHPrivate(privKeyPath))
	data := []byte(stdin())
	// 6 months default expiration is ~ 24 hours * 182.5 days
	sixmonths := time.Hour * 4380
	signed, err := cert.Sign(&privKey, data, time.Now().Add(sixmonths).Unix())
	if err != nil {
		log.Fatalln("could not sign ", err)
	}
	fmt.Println(string(signed))
}

func parseSSHPrivate(path string) *ed25519.PrivateKey {
	handle, err := os.Open(path)
	if err != nil {
		log.Fatalln("could not open "+path, err)
	}
	defer handle.Close()
	bytes, err := ioutil.ReadAll(handle)
	if err != nil {
		log.Fatalln("could not read "+path, err)
	}
	key, err := ssh.ParseRawPrivateKey(bytes)
	if err != nil {
		log.Fatalln("could not parse "+path, err)
	}
	return key.(*ed25519.PrivateKey)
}
