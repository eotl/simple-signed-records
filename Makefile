# Get the current user ID and group ID, to run the Docker containers as the host's current unprivileged user, instead of root.
UID := $(shell id -u)
GID := $(shell id -g)

CURRENT_DIR=$(patsubst %/,%,$(dir $(realpath $(firstword $(MAKEFILE_LIST)))))
ROOT_DIR=$(CURRENT_DIR)

DOCKER_COMPOSE?=docker compose

DOCKER_JS=ssr_js
DOCKER_JS_EXEC=$(DOCKER_COMPOSE) exec --user $(UID):$(GID) -it $(DOCKER_JS) sh
DOCKER_JS_EXEC_ROOT=$(DOCKER_COMPOSE) exec --user root:root -it $(DOCKER_JS) sh

DOCKER_GO=ssr_go
DOCKER_GO_EXEC=$(DOCKER_COMPOSE) exec --user $(UID):$(GID) -it $(DOCKER_GO) sh
DOCKER_GO_EXEC_ROOT=$(DOCKER_COMPOSE) exec --user root:root -it $(DOCKER_GO) sh

.PHONY: create shell shell-root dependencies packages-js dev-js serve-js build-js up login-npm publish-npm first-run start stop restart clear

# Docker Commands
create:
	export CURRENT_UID=$(UID) CURRENT_GID=$(GID); $(DOCKER_COMPOSE) up --build --no-recreate -d

shell: up
	$(DOCKER_JS_EXEC)

shell-go: up
	$(DOCKER_GO_EXEC)

shell-root: up
	$(DOCKER_JS_EXEC_ROOT)

shell-root-go: up
	$(DOCKER_GO_EXEC_ROOT)

dependencies: up
	$(DOCKER_JS_EXEC_ROOT) -c "apk add --no-cache git wget bash jq"
	$(DOCKER_GO_EXEC_ROOT) -c "apk add --no-cache git wget bash jq"
	$(DOCKER_GO_EXEC_ROOT) -c "mkdir -p /.cache && chmod -R 777 /.cache"

packages-js: up
	$(DOCKER_JS_EXEC) -c "cd javascript && yarn install --ignore-engines"

dev-go: up build-ssrd
	$(DOCKER_GO_EXEC) -c "./cmd/ssrd/ssrd -l 8089 -path ./testing/website/"

dev-js: up
	$(DOCKER_JS_EXEC) -c "cd javascript && yarn dev"

serve-js: up
	$(DOCKER_JS_EXEC) -c "cd javascript && yarn serve"

test-go: up
	$(DOCKER_GO_EXEC) -c "go test -v ./..."

test-js: up
	$(DOCKER_JS_EXEC) -c "cd javascript && yarn test"

build-ssr: up 
	$(DOCKER_GO_EXEC) -c "cd cmd/ssr/ && go build"

build-ssrd: up 
	$(DOCKER_GO_EXEC) -c "cd cmd/ssrd/ && go build"

build-js: up test-js
	rm -rf javascript/dist/*
	$(DOCKER_JS_EXEC) -c "cd javascript && yarn build"

login-npm: up
	$(DOCKER_JS_EXEC) -c "npm adduser"

publish-js: up login-npm build-js
	$(DOCKER_JS_EXEC) -c "cd javascript/ && npm publish --access=public"

up:
	export CURRENT_UID=$(UID) CURRENT_GID=$(GID); $(DOCKER_COMPOSE) up -d

# Helper Commands
first-run: create dependencies build-ssr build-ssrd packages-js dev-js

start: up dev-js

stop: $(ROOT_DIR)/docker-compose.yml
	$(DOCKER_COMPOSE) kill || true
	$(DOCKER_COMPOSE) rm --force || true

restart: stop start serve

clear: stop $(ROOT_DIR)/docker-compose.yml
	$(DOCKER_COMPOSE) down -v --remove-orphans || true
	rm -rf javascript/node_modules/
