module codeberg.org/eotl/simple-signed-records

go 1.16

require (
	github.com/BurntSushi/toml v1.0.0
	github.com/btcsuite/btcutil v1.0.2
	github.com/rs/cors v1.8.2
	github.com/stretchr/testify v1.7.0
	github.com/tyler-smith/go-bip39 v1.1.0
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
)
