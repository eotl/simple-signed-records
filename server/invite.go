package server

import (
	"bytes"
	"encoding/json"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"sync"

	"codeberg.org/eotl/simple-signed-records/crypto/cert"
	"codeberg.org/eotl/simple-signed-records/crypto/eddsa"
)

const inviteAcceptedTpl = `<html>
<head>
  <title>{{.Title}}</title>
</head>
<body>
<center>
  <h1>Invite Accepted</h1>
  <p>You may now view protected content at the following path:</p>
  <p>
    <a href="{{.Path}}">{{.Path}}</a>
  </p>
</center>
</body>
</html>
`

const inviteRejectedTpl = `<html>
<head>
  <title>{{.Title}}</title>
</head>
<body>
<center>
  <h1>Invite Invalid</h1>
</center>
</body>
</html>
`

type inviteData struct {
	Title  string `json:"title,omitempty"`
	Status int    `json:"status"`
	Path   string `json:"path"`
}

var inviteAccepted, _ = template.New("accept").Parse(inviteAcceptedTpl)
var inviteRejected, _ = template.New("reject").Parse(inviteRejectedTpl)

type InviteCode struct {
	Token string `json:"token"`
	Path  string `json:"path"`
}

type InviteResponse struct {
	Token string `json:"token"`
	Nonce string
}

// InviteService implements the http.Handler for redeeming invite codes into an Authorization entry
type InviteService struct {
	InviteDir string
	Auth      Authorization
	replyJSON bool
	sync.Mutex
}

func (i *InviteService) respond(w http.ResponseWriter, status int, res string) {
	txt := http.StatusText(status)
	s := inviteData{Status: status, Path: res}
	if i.replyJSON {
		w.Header().Set("content-type", "application/json")
	}
	w.WriteHeader(status)

	if i.replyJSON {
		b, err := json.Marshal(s)
		if err == nil {
			w.Write(b)
		}
	} else {
		s.Title = txt
		if status == http.StatusCreated {
			inviteAccepted.Execute(w, s)
		} else {
			inviteRejected.Execute(w, s)
		}
	}
}

func (i *InviteService) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request: %s", r.URL.Path)
	i.Lock()
	defer i.Unlock()
	// read maximum 1kB of payload
	c, err := io.ReadAll(io.LimitReader(r.Body, 1<<10))
	log.Println(string(c))
	if err == nil {
		// extract the signature identities and signed data
		certified, verifiers, err := verifySelfSigned(c)
		if err != nil {
			log.Println(err)
			i.respond(w, http.StatusForbidden, "")
			return
		}
		if err == nil {
			if len(verifiers) != 1 {
				// invalid
				log.Println("Only one Signature on InviteResponse is supported")
				i.respond(w, http.StatusForbidden, "")
				return
			}
			// extract the invite token from the certified data
			de := json.NewDecoder(bytes.NewBuffer(certified))
			ir := new(InviteResponse)
			err = de.Decode(ir)
			if err != nil {
				log.Println(err)
				i.respond(w, http.StatusForbidden, "")
				return
			}

			// try to find the invite in InviteDir
			p := path.Join(i.InviteDir, ir.Token)
			f, err := os.Open(p)
			if err != nil {
				// return failure type
				log.Println("Invalid invite", ir.Token)
				i.respond(w, http.StatusForbidden, "")
				return
			}
			authB, err := io.ReadAll(f)
			if err != nil {
				log.Println("Unable to read invitePath", p)
				i.respond(w, http.StatusForbidden, "")
				return
			}
			res := string(authB)

			// remove invite before releasing lock
			defer os.Remove(p)

			// try to add the authorization
			pubKey := new(eddsa.PublicKey)
			pubKey.FromString(verifiers[0].Identity())
			i.Auth.Add(pubKey, res, ModeRead)
			log.Println("Added", pubKey, res, "ModeRead")
			i.respond(w, http.StatusCreated, res)
		}
		log.Println(err)
	}
}

func getVerifiers(rawCert []byte) ([]cert.Verifier, error) {
	sigs, err := cert.GetSignatures(rawCert)
	if err != nil {
		return nil, err
	}

	v := make([]cert.Verifier, len(sigs))
	for i, sig := range sigs {
		pubKey := new(eddsa.PublicKey)
		err = pubKey.FromString(sig.Identity)
		if err != nil {
			return nil, err
		}
		v[i] = pubKey
	}
	return v, nil
}

func verifySelfSigned(rawCert []byte) ([]byte, []cert.Verifier, error) {
	var certified []byte
	var err error

	verifiers, err := getVerifiers(rawCert)
	if err != nil {
		return nil, nil, err
	}
	// Note: all verifiers must Verify
	for _, v := range verifiers {
		certified, err = cert.Verify(v, rawCert)
		if err != nil {
			return nil, nil, err
		}
	}

	return certified, verifiers, nil
}
