package server

import (
	"errors"
	"github.com/BurntSushi/toml"
)

// Config contains configuration options. The `toml:"keyname"` tags correspond to configuration line entries.
type Config struct {
	// CORSMethods lists the accepted request origin. e.g. cors_origins = [ "*" ]
	CORSOrigins []string `toml:"cors_origins"`

	// CORSMethods lists the accepted http methods. e.g. cors_methods = [ "GET", "POST" ]
	CORSMethods []string `toml:"cors_methods"`

	// Dirlist enables directory listing mode
	Dirlist bool `toml:"dirlist"`

	// HTTPResponses configures the content type of http error responses. Valid entries are "json"
	HTTPResponses string `toml:"http_responses"`

	// Listening port
	HTTPPort int `toml:"http_port"`

	// ProxyPort specifies the proxy listening port, disabled by default
	ProxyPort int `toml:"proxy_port"`

	// ProxyTo specifies the URL of the upstream host to proxy requests for, e.g. http://127.0.0.1")
	ProxyTo string `toml:"proxy_to"`

	// InviteDir specifies the location of invite token files
	InviteDir string `toml:"invite_dir"`

	// AutocertDomains lists the domains for which to enable TLS
	// listeners using LetsEncrypto ACME autocert
	AutocertDomains []string `toml:"autocert_domains"`
}

// LoadConfig loads configuration from filename
func LoadConfig(filename string) (*Config, error) {
	c := &Config{}
	_, e := toml.DecodeFile(filename, c)

	if e != nil {
		return nil, e
	}

	return c, nil
}

func (c *Config) Validate() error {
	// if CORS is configured, both origins and methods must be specified
	nOrigins := len(c.CORSOrigins)
	nMethods := len(c.CORSMethods)
	if (nOrigins != 0 || nMethods != 0) && !(nOrigins != 0 && nMethods != 0) {
		return errors.New("Both cors_origins and cors_methods must be specified")
	}

	// at least one service must be configured
	if c.ProxyPort == 0 && c.HTTPPort == 0 {
		return errors.New("Either proxy_port or http_port must be configured")
	}
	// proxyTo must also be specified with proxyPort
	if c.ProxyTo != "" && c.ProxyPort == 0 {
		return errors.New("ProxyTo must be configured along with ProxyPort")
	}
	return nil
}
