// server_test.go - simple-signed-records http server tests.
// Copyright (C) 2022 Masala.
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package server

import (
	"codeberg.org/eotl/simple-signed-records/crypto/eddsa"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"testing"
	"time"
)

func TestAuthenticate(t *testing.T) {
	require := require.New(t)
	// make tempdir
	d, e := ioutil.TempDir("", "ssr_listen_tests")
	require.NoError(e)
	defer os.RemoveAll(d)

	// make a file to serve
	tmpf, e := ioutil.TempFile(d, "")
	require.NoError(e)
	b := make([]byte, 42)
	n, e := rand.Reader.Read(b)
	require.NoError(e)
	require.Equal(n, 42)
	enc := base64.NewEncoder(base64.StdEncoding, tmpf)
	enc.Write(b)
	e = enc.Close()
	require.NoError(e)

	// make a new userkey
	pk, e := eddsa.NewKeypair(rand.Reader)
	require.NoError(e)

	// create auth.db
	authF := path.Join(d, "auth.db")
	f, e := os.Create(authF)
	require.NoError(e)
	dir, fn := path.Split(tmpf.Name())
	require.Equal(path.Clean(dir), d)
	_, e = fmt.Fprintf(f, "%s /%s r\n", pk.Identity(), fn)
	require.NoError(e)
	f.Close()

	// load authFile
	p := new(AuthFile)
	e = p.Load(authF)
	require.NoError(e)

	// config
	c := &Config{HTTPPort: 9999}

	// start the server
	h, e := New(d, p, c)
	require.NoError(e)

	// ListenAndServe is called in another thread, so must wait for it to start..
	time.Sleep(1 * time.Second)

	// construct a url for the filename at siteroot
	u, e := url.Parse("http://127.0.0.1:9999" + "/" + fn)
	require.NoError(e)

	// Craft a request for the protected resource
	r, e := http.NewRequest("GET", u.String(), nil)
	require.NoError(e)

	// Instantiate a normal http.Client and verify request fails
	httpClient := &http.Client{}
	re, e := httpClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusUnauthorized)

	// Instantiate an SSRClient and verify request succeeds
	ssrClient := NewClient(pk)
	re, e = ssrClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusOK)

	// read and compare page body
	b, e = ioutil.ReadAll(re.Body)
	require.NoError(e)
	tmpf.Seek(0, 0)
	bb, e := ioutil.ReadAll(tmpf)
	require.NoError(e)
	require.Equal(b, bb)

	// create new tmpfile
	tmpf, e = ioutil.TempFile(dir, "")
	fmt.Fprintf(tmpf, "some file contents")
	require.NoError(e)

	// Add a new rule
	e = p.Add(pk.PublicKey(), "/"+path.Base(tmpf.Name()), ModeRead)
	require.NoError(e)

	// construct a url for the filename at siteroot
	u, e = url.Parse("http://127.0.0.1:9999" + "/" + path.Base(tmpf.Name()))
	require.NoError(e)

	// Craft a request for the protected resource
	r, e = http.NewRequest("GET", u.String(), nil)
	require.NoError(e)

	// verify request succeeds
	re, e = ssrClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusOK)

	// reload authF
	wtf, e := ioutil.ReadFile(authF)
	require.NoError(e)

	t.Logf(string(wtf))
	e = p.Load(authF)
	require.NoError(e)

	// verify request succeeds
	re, e = ssrClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusOK)

	// shut it all down
	h.Shutdown()
	h.Wait()
}

func TestWildard(t *testing.T) {
	require := require.New(t)
	// make tempdir for wwwroot
	wwwroot, e := ioutil.TempDir("", "ssr_listen_tests")
	require.NoError(e)
	defer os.RemoveAll(wwwroot)

	// make another tempdir inside wwwroot
	d, e := ioutil.TempDir(wwwroot, "")

	// make a file to serve
	tmpf, e := ioutil.TempFile(d, "")
	require.NoError(e)
	b := make([]byte, 42)
	n, e := rand.Reader.Read(b)
	require.NoError(e)
	require.Equal(n, 42)
	enc := base64.NewEncoder(base64.StdEncoding, tmpf)
	enc.Write(b)
	e = enc.Close()
	require.NoError(e)

	// make a new userkey
	pk, e := eddsa.NewKeypair(rand.Reader)
	require.NoError(e)

	// create auth.db
	authF := path.Join(d, "auth.db")
	f, e := os.Create(authF)
	require.NoError(e)

	// chop the wwwroot part of the path off tempdir d and add a wildcard permission to the authdb
	dir, fn := path.Split(tmpf.Name())
	_, e = fmt.Fprintf(f, "%s /%s/* r\n", pk.Identity(), path.Base(dir))
	require.NoError(e)
	f.Close()

	// load authFile
	p := new(AuthFile)
	e = p.Load(authF)
	require.NoError(e)

	// default config
	c := &Config{HTTPPort: 9999}

	// start the server
	h, e := New(wwwroot, p, c)
	require.NoError(e)

	// ListenAndServe is called in another thread, so must wait for it to start..
	time.Sleep(1 * time.Second)

	// construct a url for the file inside dir under siteroot
	dir, fn = path.Split(tmpf.Name())
	u, e := url.Parse("http://127.0.0.1:9999" + "/" + path.Base(dir) + "/" + fn)
	require.NoError(e)

	// Craft a request for the protected resource
	r, e := http.NewRequest("GET", u.String(), nil)
	require.NoError(e)

	// Instantiate a normal http.Client and verify request fails
	httpClient := &http.Client{}
	re, e := httpClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusUnauthorized)

	// Instantiate an SSRClient and verify request succeeds
	ssrClient := NewClient(pk)
	re, e = ssrClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusOK)

	// read and compare page body
	b, e = ioutil.ReadAll(re.Body)
	require.NoError(e)
	tmpf.Seek(0, 0)
	bb, e := ioutil.ReadAll(tmpf)
	require.NoError(e)
	require.Equal(b, bb)

	// construct a url for a missing file inside authorized dir under siteroot
	u, e = url.Parse("http://127.0.0.1:9999" + "/" + path.Base(dir) + "/" + "foo")
	require.NoError(e)

	// Craft a request for the protected resource
	r, e = http.NewRequest("GET", u.String(), nil)
	require.NoError(e)

	// Instantiate a normal http.Client and verify request fails
	re, e = httpClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusUnauthorized)

	// Instantiate an SSRClient and verify request failes
	re, e = ssrClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusNotFound)

	// shut it all down
	h.Shutdown()
	h.Wait()
}

func TestGlobbing(t *testing.T) {
	require := require.New(t)
	// make tempdir for wwwroot
	wwwroot, e := ioutil.TempDir("", "ssr_listen_tests")
	require.NoError(e)
	defer os.RemoveAll(wwwroot)

	// make another tempdir inside wwwroot
	d, e := ioutil.TempDir(wwwroot, "")

	// make a file to serve ending in .txt
	tmpf, e := ioutil.TempFile(d, "*.txt")
	require.NoError(e)

	// make another file in same direcotry that doesn't match the glob rule
	tmpf2, e := ioutil.TempFile(d, "")
	require.NoError(e)

	// files cannot be empty so write some random data into each
	b := make([]byte, 42)
	n, e := rand.Reader.Read(b)
	require.NoError(e)
	require.Equal(n, 42)
	enc := base64.NewEncoder(base64.StdEncoding, tmpf)
	enc.Write(b)
	e = enc.Close()
	require.NoError(e)

	enc = base64.NewEncoder(base64.StdEncoding, tmpf2)
	enc.Write(b)
	e = enc.Close()
	require.NoError(e)

	// make a new userkey
	pk, e := eddsa.NewKeypair(rand.Reader)
	require.NoError(e)

	// create auth.db
	authF := path.Join(d, "auth.db")
	f, e := os.Create(authF)
	require.NoError(e)

	// chop the wwwroot part of the path off tempdir d and add a wildcard glob (*.txt) permission to the authdb
	dir, fn := path.Split(tmpf.Name())
	_, e = fmt.Fprintf(f, "%s /%s/*.txt r\n", pk.Identity(), path.Base(dir))
	require.NoError(e)
	f.Close()

	// load authFile
	p := new(AuthFile)
	e = p.Load(authF)
	require.NoError(e)

	// default config
	c := &Config{HTTPPort: 9999}

	// start the server
	h, e := New(wwwroot, p, c)
	require.NoError(e)

	// ListenAndServe is called in another thread, so must wait for it to start..
	time.Sleep(1 * time.Second)

	// construct a url for the file inside dir under siteroot
	dir, fn = path.Split(tmpf.Name())
	u, e := url.Parse("http://127.0.0.1:9999" + "/" + path.Base(dir) + "/" + fn)
	require.NoError(e)

	// Craft a request for the protected resource
	r, e := http.NewRequest("GET", u.String(), nil)
	require.NoError(e)

	// Instantiate a normal http.Client and verify request fails
	httpClient := &http.Client{}
	re, e := httpClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusUnauthorized)

	// Instantiate an SSRClient and verify request succeeds
	ssrClient := NewClient(pk)
	re, e = ssrClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusOK)

	// construct a url for a file that doesn't match glob rule but is in same directory
	dir, fn = path.Split(tmpf2.Name())
	u, e = url.Parse("http://127.0.0.1:9999" + "/" + path.Base(dir) + "/" + fn)
	require.NoError(e)

	// Craft a request for the protected resource
	r, e = http.NewRequest("GET", u.String(), nil)
	require.NoError(e)

	// Instantiate a normal http.Client and verify request succeeds
	re, e = httpClient.Do(r)
	require.NoError(e)
	require.Equal(re.StatusCode, http.StatusOK)

	// shut it all down
	h.Shutdown()
	h.Wait()
}
