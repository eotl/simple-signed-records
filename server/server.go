// server.go - simple-signed-records http server.
// Copyright (C) 2022 Masala.
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package server

import (
	"bufio"
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"golang.org/x/crypto/acme/autocert"
	"html/template"
	"io"
	"log"
	"mime"
	"net"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"codeberg.org/eotl/simple-signed-records/crypto/cert"
	"codeberg.org/eotl/simple-signed-records/crypto/eddsa"

	"github.com/rs/cors"
)

const statusTpl = `<html>
<head>
    <title>{{.Title}}</title>
</head>
<body>
    <h1>{{.Title}}</h1>
    <p>SSR Sever Status: <a href="/_help/{{ .Status }}">{{ .Message }}</a></p>
</body>
</html>
`

type statusData struct {
	Title   string `json:"title,omitempty`
	Status  int    `json:"status"`
	Message string `json:"message"`
	Path    string `json:"path,omitempty`
}

var httpStatus, _ = template.New("status").Parse(statusTpl)

func (h *HTTPService) respond(w http.ResponseWriter, status int) {
	txt := http.StatusText(status)
	s := statusData{Status: status, Message: txt}
	if h.replyJSON {
		w.Header().Set("content-type", "application/json")
	}
	w.WriteHeader(status)
	if h.replyJSON {
		b, err := json.Marshal(s)
		if err == nil {
			w.Write(b)
		}
	} else {
		s.Title = txt
		httpStatus.Execute(w, s)
	}
}

// HTTPService is a HTTP service
type HTTPService struct {
	log        *log.Logger
	haltOnce   sync.Once
	haltedCh   chan interface{}
	fatalErrCh chan error
	address    string
	staticPath string
	httpServer *http.Server
	mux        *http.ServeMux
	auth       Authorization
	mu         sync.RWMutex
	challenges map[string]struct{}
	cfg        *Config
	replyJSON  bool
}

// ProxyService is an authenticating HTTP Proxy that forwards requests to another HTTP listener if authenticated.
// HTTPService is a HTTP service
type ProxyService struct {
	HTTPService
	proxyto *url.URL
	client  *http.Client
	//proxypin string // the fingerprint of a certificate that we will pin to
}

// ServeHTTP implements the http.Handler interface for HTTPService to route requests.
func (h *ProxyService) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	h.log.Printf("Request: %s", r.URL.Path)

	/*
		if r.URL.Host != h.proxyForHost {
			// invalid request; we only proxy to specified host
			h.respond(http.StatusForbidden)
			return
		}
	*/

	// rewrite the request URL
	req := r.Clone(context.Background())

	// rewrite the request URL to our proxy endpoint
	req.URL.Host = h.proxyto.Host

	// set the request URL scheme (r.URL.Scheme is "" ...)
	req.URL.Scheme = h.proxyto.Scheme

	// unset RequestURI
	req.RequestURI = ""

	// verify the request
	_, err := h.verifyRequest(r, w)
	if err != nil {
		h.log.Println(err)
		return
	}

	// make the request
	resp, err := h.client.Do(req)

	if err != nil {
		// failed to get a response
		return
	}

	// stream the response to client
	io.Copy(w, resp.Body)
	return
}

// SSRClient is an http.Client that authenticates requests with simple-signed-records via the http.RoundTripper interface
type SSRClient struct {
	http.Client
}

// NewClient returns a SSRClient that authentiates requests with authKey
func NewClient(authKey *eddsa.PrivateKey) *SSRClient {
	t := new(SSRClient)
	t.Transport = &SSRTransport{authKey: authKey}
	return t
}

// SSRTransport implements http.RoundTripper
type SSRTransport struct {
	authKey *eddsa.PrivateKey
}

// RoundTrip makes a request, and if it returns http.StatusUnauthorized (401) it retries the request, authenticating with the authKey
func (t *SSRTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	r := req.Clone(req.Context())
	// do request and check if authentication is required
	transport := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		ForceAttemptHTTP2:     true,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
		DisableCompression:    true,
	}
	re, err := transport.RoundTrip(r)
	if err != nil {
		return re, err
	}
	if re.StatusCode == http.StatusUnauthorized {
		ch := re.Header.Get("WWW-Authenticate")
		a := new(SSRAuth)
		de := json.NewDecoder(bytes.NewBuffer([]byte(ch)))
		e := de.Decode(a)
		if e != nil {
			return re, e
		}
		if a.Challenge == "" {
			return re, errors.New("Invalid Challenge")
		}

		// generate a unique nonce for this response
		b := make([]byte, 32)
		n, e := io.ReadFull(rand.Reader, b)
		if n != 32 || e != nil {
			log.Fatalln("Unable to get sufficient entropy")
		}
		a.Nonce = string(b)
		a.Path = req.URL.Path

		// serialize the response
		p, e := json.Marshal(a)
		if e != nil {
			log.Fatalln("Unable to marshal response")
		}

		// sign the response
		cr, e := cert.Sign(t.authKey, p, time.Now().Add(time.Second*60).Unix())
		if e != nil {
			log.Fatalln("Unable to sign certificate")
		}

		// send the signed request
		r.Header.Add("SSR-Authenticate", base64.StdEncoding.EncodeToString(cr))
		return transport.RoundTrip(r)

	} else {
		return re, err
	}
}

// a struct to return a verified request
type verifiedRequest struct {
	identity *eddsa.PublicKey
	path     string
}

// the authtoken is a signed json encoded struct with challenge and path fields.
type SSRAuth struct {
	Nonce     string `json:omitempty`
	Path      string `json:omitempty`
	Challenge string
}

func getChallenge() string {
	b := make([]byte, 32)
	n, err := io.ReadFull(rand.Reader, b)
	if n != len(b) || err != nil {
		panic(errors.New("Unable to create challenge"))
	}
	return base64.StdEncoding.EncodeToString(b)
}

func (h *HTTPService) failure(r http.ResponseWriter, status int) {
	c := getChallenge()
	h.mu.Lock()
	h.challenges[c] = struct{}{}
	h.mu.Unlock()
	a := &SSRAuth{Challenge: c}
	au, e := json.Marshal(a)
	if e != nil {
		panic(e)
	}
	r.Header().Add("WWW-Authenticate", string(au))
	h.respond(r, status)
}

func (h *HTTPService) verifyRequest(request *http.Request, response http.ResponseWriter) (*verifiedRequest, error) {
	v := &verifiedRequest{}

	// check for the SSR-Authenticate client header
	sa := request.Header.Get("Ssr-Authenticate")
	if sa == "" {
		h.log.Printf("No Ssr-Authenticate in %s", request.Header)
		// if there is no authentication header, see if the request is allowed
		if h.auth.Allowed(nil, request.URL.Path, ModeRead) {
			v.identity = nil
			v.path = request.URL.Path
			return v, nil
		}
		// add challenge to valid challenges ?
		h.failure(response, http.StatusUnauthorized)
		return nil, errors.New("Failure")
	}
	// decode base64 string
	b, err := base64.StdEncoding.DecodeString(sa)
	if err != nil {
		return nil, err
	}
	h.log.Printf("Deserialized SSR-Authenticate: %s", b)

	// extract the signature from the authentication header
	signatures, err := cert.GetSignatures(b)
	if err != nil {
		h.failure(response, http.StatusBadRequest)
		return nil, err
	}

	// Initialize a PublicKey
	pubKey := new(eddsa.PublicKey)
	sig := signatures[0] // just uses the first signature, and we only expect one.
	err = pubKey.FromString(sig.Identity)
	if err != nil {
		h.failure(response, http.StatusBadRequest)
		return nil, err
	}

	// Verify the certificate is signed with the embedded key
	certified, err := cert.Verify(pubKey, b)
	if err != nil {
		h.failure(response, http.StatusBadRequest)
		h.log.Printf("Certificate from %s invalid", pubKey.Identity())
		return nil, err
	}

	// the challenge/response pair must be a valid challenge and a path
	a := &SSRAuth{}
	d := json.NewDecoder(bytes.NewBuffer(certified))
	err = d.Decode(a)
	if err != nil {
		h.failure(response, http.StatusBadRequest)
		h.log.Printf("Challenge invalid: %s", err)
		return nil, err // failure to authenticate
	}

	// verify that the challenge is valid, and expunge from valid challenges!
	h.mu.RLock()
	if _, ok := h.challenges[a.Challenge]; !ok {
		h.mu.RUnlock()
		h.failure(response, http.StatusBadRequest)
		return nil, errors.New("Challenge rejected")
	} else {
		h.mu.RUnlock()
		h.mu.Lock()
		delete(h.challenges, a.Challenge)
		h.mu.Unlock()
		h.log.Printf("Challenge accepted: %s", a.Challenge)
	}

	// verify that the request path matches the authenticated request path
	if request.URL.EscapedPath() != a.Path {
		h.log.Printf("%v != %v", request.URL.EscapedPath(), a.Path)
		h.failure(response, http.StatusBadRequest)
		return nil, errors.New("Requested URLPath does not match certified path")
	}

	// verify that the public key is allowed to request this path
	if request.Method != "GET" && request.Method != "HEAD" {
		h.failure(response, http.StatusBadRequest)
		return nil, errors.New("Only GET and HEAD are implemented at this time")
	}
	if !h.auth.Allowed(pubKey, a.Path, ModeRead) {
		h.log.Printf("%s %s %v", pubKey, a.Path, ModeRead)
		h.failure(response, http.StatusForbidden)
		return nil, errors.New("Unauthorized identity")
	}
	v.identity = pubKey
	v.path = a.Path
	return v, nil
}

// ServeHTTP implements the http.Handler interface for HTTPService to route requests.
func (h *HTTPService) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request: %s", r.URL.Path)

	_, err := h.verifyRequest(r, w)
	if err != nil {
		return
	}

	p := filepath.Clean(r.URL.Path)
	p = filepath.Join(h.staticPath, p)
	fi, err := os.Stat(p)
	if err != nil {
		// PathError; rejecting
		log.Printf("Invalid path: %s", err)
		h.respond(w, http.StatusNotFound)
		return
	}

	// set content-length from file descriptor
	w.Header().Set("content-length", strconv.FormatInt(fi.Size(), 10))

	if r.Method == "HEAD" {
		h.respond(w, http.StatusOK)
		return
	}

	if fi.IsDir() {
		// preferentially serve index.html or index.htm  if it exists
		idx := filepath.Join(p, "index.html")
		_, err = os.Stat(idx)
		if err != nil {
			idx := filepath.Join(p, "index.htm")
			_, err = os.Stat(idx)
			if err != nil {
				// else if dirlisting is enabled, serve using FileServer
				if h.cfg.Dirlist {
					http.ServeFile(w, r, p)
					return
				} else {
					log.Printf("Denying directory read: %s", r.URL.Path)
					h.respond(w, http.StatusNotFound)
					return
				}
			} else {
				p = idx
			}
		} else {
			p = idx
		}
	}

	// determine the mimetype to set the content-type header
	ext := strings.ToLower(filepath.Ext(p))
	mimetype := mime.TypeByExtension(ext)
	f, err := os.Open(p)
	if err != nil {
		log.Printf("Unable to open: %s", p)
		h.respond(w, http.StatusNotFound)
		return
	}

	// try to detect the mimetype
	if mimetype == "" {
		buf := make([]byte, 512)
		n, err := f.Read(buf)
		if err != nil {
			h.respond(w, http.StatusNotFound)
			return
		}
		mimetype = http.DetectContentType(buf[:n])
		f.Seek(0, 0)
	}

	w.Header().Set("content-type", mimetype)
	// if mimetype isn't text or json, suggest a filename
	if mimetype != "application/json" && mimetype != "text/plain" && !strings.HasPrefix(mimetype, "text/html") {
		w.Header().Set("content-disposition", filepath.Base(p))
	}

	// stream response to client
	io.Copy(w, f)
	return
}

type FileMode uint8

const (
	ModeRead = iota + 1
	ModeWrite
)

// Authorization is the interface via which a public identity key can be checked or added to specific paths.
type Authorization interface {
	Allowed(idKey *eddsa.PublicKey, path string, mode FileMode) bool
	Add(idKey *eddsa.PublicKey, path string, mode FileMode) error
	Remove(idKey *eddsa.PublicKey, path string, mode FileMode) error
}

// AuthFile is a file-backed implementation of Authorization
type AuthFile struct {
	m  map[string]map[string]FileMode
	p  map[string]bool
	mu sync.RWMutex
	fn string
}

// Load reads authorizations from disk. The format is a simple space separated entry per line:
// bech32 encoded public identity key, a relative path from siteroot, flags rw
// Note, currently only "r" flag is supported, any other flag is an error.
// e.g. ssi1vghn3z9rwupvg202qeu35ukzrrtr59gnxq9cvwp6mlev637vngsqtw86ha /foofile r
// e.g. ssi1vghn3z9rwupvg202qeu35ukzrrtr59gnxq9cvwp6mlev637vngsqtw86ha /foo/* r // files inside of /foo only
// e.g. ssi1vghn3z9rwupvg202qeu35ukzrrtr59gnxq9cvwp6mlev637vngsqtw86ha /foo/** r // recursively everything under the /foo path
func (s *AuthFile) Load(filename string) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.fn = filename
	f, e := os.Open(filename)
	if e != nil {
		return e
	}
	s.m = make(map[string]map[string]FileMode)
	s.p = make(map[string]bool)

	sc := bufio.NewScanner(f)
	counter := 0
	for sc.Scan() {
		fields := strings.Split(sc.Text(), " ")
		if len(fields) != 3 {
			return errors.New("Invalid auth file")
		}
		bechKey, pth, rw := fields[0], fields[1], fields[2]
		s.p[path.Clean(pth)] = true
		var mode FileMode
		if strings.Contains(rw, "r") {
			mode |= ModeRead
		}
		if strings.Contains(rw, "w") {
			mode |= ModeWrite
		}

		// TODO validate key !
		// create map for each key
		if _, ok := s.m[bechKey]; !ok {
			s.m[bechKey] = make(map[string]FileMode)
		}
		// populate each authorization in map
		s.m[bechKey][pth] = mode
		counter++
	}
	log.Printf("ACL rules loaded: %d", counter)
	return nil
}

// Allowed implements the Authorization interface for checking an authorization
func (s *AuthFile) Allowed(idKey *eddsa.PublicKey, pth string, mode FileMode) bool {
	s.mu.RLock()
	defer s.mu.RUnlock()

	// check all path rulesets to see if authorization required
	// any wildcard rule enables authentication requirement for paths under it.
	// for example:
	// idkey /foo/* r
	//  would also require authorization to access /foo/bar/baz
	// however, idkey would not have authorization to access /foo/bar/baz
	// recursive wildcard rules enable access for all paths matching the rule:, for example
	// idkey /foo/** r enables authorization requirement for all paths under /foo and allows idkey access to all of these paths.
	needauth := false
	p := path.Clean(pth)
	if strings.HasPrefix(p, "/.ssr") {
		needauth = true
	}
	for r, _ := range s.p {
		// if rule is a wildcard rule, any more specific path must be authenticated
		if strings.HasSuffix(r, "*") {
			d, _ := path.Split(r)                   // consider directory and all child paths protected, drop the filename component (/*, /**)
			if strings.Contains(p, path.Clean(d)) { // protected directory is within the path of request, therefore authorization required.
				needauth = true
				break
			}

		// check to see if the request is a more specific path under a protected path
		} else if strings.Contains(p, r) {
			needauth = true
			break
		// check to see if the ACL rule matches the request path
		} else {
			m, err := filepath.Match(r, p)
			if m && err == nil {
				needauth = true
				break
			}
		}
	}

	// no authorization was required for this path
	if !needauth {
		return true
	}

	if idKey == nil {
		return false
	}

	// identity has an entry in auth db
	if acl, ok := s.m[idKey.Identity()]; ok {

		// exact path match
		if m, ok := acl[p]; ok && (m&mode > 0) {
			return true
		}

		d := path.Dir(p)

		// wildcard match for files in a path
		if m, ok := acl[d+"/*"]; ok && (m&mode > 0) {
			return true
		}
		// recursive wildcard match in path
		for r, _ := range acl {
			if strings.HasSuffix(r, "**") {
				// drop the filename and slash
				if strings.Contains(d, path.Dir(r)) {
					return true
				}
			}

			// allow access to files that match the access rule
			m, err := filepath.Match(r, p)
			if m && err == nil {
				return true
			}
		}
	}

	return false
}

// Add implements the Authorization interface for adding an authorization to the AuthFile
func (s *AuthFile) Add(idKey *eddsa.PublicKey, path string, mode FileMode) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	w, err := os.OpenFile(s.fn, os.O_APPEND|os.O_RDWR, 0660)
	if err != nil {
		return err
	}
	bechKey := idKey.Identity()
	fmt.Fprintf(w, "%s %s r\n", bechKey, path)
	w.Close()
	// create map for each key
	if _, ok := s.m[bechKey]; !ok {
		s.m[bechKey] = make(map[string]FileMode)
	}
	// populate each authorization in map
	s.m[bechKey][path] = ModeRead

	return nil
}

// Remove implements the Authorization interface for removing an authorization from the AuthFile
func (s *AuthFile) Remove(idKey *eddsa.PublicKey, path string, mode FileMode) error {
	return errors.New("NotImplemented")
}

// NewProxy returns a new ProxyService for forwarding authenticated HTTP requests
func NewProxy(config *Config, auth Authorization) (*ProxyService, error) {

	l := log.New(os.Stdout, "", log.Ldate|log.LUTC|log.Ltime)

	u, err := url.Parse(config.ProxyTo)
	if err != nil {
		l.Fatalln(err)
	}

	// additionally ProxyTo, must be http:// or https://
	if !(u.Scheme == "http" || u.Scheme == "https") {
		l.Fatalln(errors.New("ProxyTo must be of form http://hostname or https://hostname"))
	}

	h := &ProxyService{}
	h.address = ":" + strconv.Itoa(config.ProxyPort)
	h.log = l
	h.haltedCh = make(chan interface{})
	h.fatalErrCh = make(chan error)
	h.auth = auth
	h.challenges = make(map[string]struct{})
	h.proxyto = u
	h.client = &http.Client{}
	h.mux = http.NewServeMux()
	h.mux.Handle("/", h)
	h.httpServer = &http.Server{
		Addr:     h.address,
		Handler:  h.mux,
		ErrorLog: h.log,
	}

	// Start the daemon
	h.log.Println("Proxying requests to: " + config.ProxyTo)
	go func() {
		h.log.Println("Starting HTTPService")
		var err error
		if len(config.AutocertDomains) > 0 {
			err = http.Serve(autocert.NewListener(config.AutocertDomains...), h)
		} else {
			err = h.httpServer.ListenAndServe()
		}
		if err != http.ErrServerClosed {
			h.log.Printf("Failed to start/stop HTTP listener with %v\n", err)
		}
	}()

	return h, nil
}

// New returns a new HttpService
func New(staticPath string, auth Authorization, config *Config) (*HTTPService, error) {
	// verify that staticPath exists and is a directory
	fi, err := os.Stat(staticPath)
	if err != nil {
		return nil, err
	}

	if !fi.IsDir() {
		return nil, errors.New("staticPath is not a directory")
	}

	l := log.New(os.Stdout, "", log.Ldate|log.LUTC|log.Ltime)

	h := &HTTPService{
		address:    ":" + strconv.Itoa(config.HTTPPort),
		staticPath: staticPath,
		log:        l,
		haltedCh:   make(chan interface{}),
		fatalErrCh: make(chan error),
		auth:       auth,
		challenges: make(map[string]struct{}),
		cfg:        config,
	}

	// respond to requests with json in response body
	if strings.ToLower(config.HTTPResponses) == "json" {
		h.replyJSON = true
	}

	h.mux = http.NewServeMux()
	if len(config.CORSOrigins) > 0 {
		c := cors.New(cors.Options{
			AllowedOrigins: config.CORSOrigins,
			AllowedMethods: config.CORSMethods,
		})
		h.mux.Handle("/", c.Handler(h))
		if h.cfg.InviteDir != "" {
			i := &InviteService{InviteDir: h.staticPath + "/.ssr/" + h.cfg.InviteDir, Auth: auth, replyJSON: h.replyJSON}
			h.mux.Handle("/redeem_token", c.Handler(i))
		}

	} else {
		h.mux.Handle("/", h)
		if h.cfg.InviteDir != "" {
			i := &InviteService{InviteDir: h.staticPath + "/.ssr/" + h.cfg.InviteDir, Auth: auth, replyJSON: h.replyJSON}
			// respond to requests with json in response body
			h.mux.Handle("/redeem_token", i)
		}
	}

	h.httpServer = &http.Server{
		Addr:     h.address,
		Handler:  h.mux,
		ErrorLog: h.log,
	}

	// Start the daemon
	h.log.Println("Serving: " + staticPath)
	go func() {
		h.log.Println("Starting HTTPService")
		if len(config.AutocertDomains) > 0 {
			err = http.Serve(autocert.NewListener(config.AutocertDomains...), h)
		} else {
			err = h.httpServer.ListenAndServe()
		}
		if err != http.ErrServerClosed {
			h.log.Printf("Failed to start/stop HTTP listener with %v\n", err)
		}

	}()

	return h, nil
}

// Wait blocks until Shutdown has been called
func (h *HTTPService) Wait() {
	<-h.haltedCh
}

// Shutdown halts the HTTPService and closes all listeners
func (h *HTTPService) Shutdown() {
	h.haltOnce.Do(func() { h.halt() })
	return
}

func (h *HTTPService) halt() {
	h.log.Println("Halting HTTPService...")
	h.httpServer.Shutdown(context.Background())
	close(h.haltedCh)
}
